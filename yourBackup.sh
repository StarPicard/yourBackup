#! /bin/sh
# Name: yourBackup.sh
# Save your digital life

# BEGIN CONFIG ---------------------------------------------------------
LNKCNT=1
HOSTNAME=$(sed -n '1p' /etc/hostname)
USER=$(whoami)
EXCLUDE="./yourBackup/exclude_$USER@$HOSTNAME.cfg"
# Demo Mode
DEMO=0
#Max days to go back
MAXLINKCNT=60
#Todays date in ISO-8601 format:
DAY0=$(date -I)
#Yesterdays date in ISO-8601 format:
DAYLNK=$(date -I -d "$LNKCNT day ago")
#The source directory:
SRC="/home/$USER/"
#The target directory:
TRG="./files/$USER@$HOSTNAME/"
#rsync options
OPT+=("-avh")
OPT+=("--delete")
OPT+=("--exclude-from=$EXCLUDE")
OPT+=("--progress")
OPT+=("--stats")
# END CONFIG -----------------------------------------------------------

echo "Watching for older backups to link..."
if test -d "$TRG$DAY0"
then
	echo "Backup already made today..."
	exit 5
else
	for (( i=LNKCNT; i <= $MAXLINKCNT; i++ ))
	do
		if test -d "$TRG$DAYLNK"
		then
			break
		fi
		LNKCNT=$i
		DAYLNK=$(date -I -d "$LNKCNT day ago")
	done
fi

if test $LNKCNT -eq $MAXLINKCNT
then
	echo "No Backup newer than $MAXLINKCNT days found"
else
	echo "Last backup was $LNKCNT days ago"
	echo "Linking against $TRG$DAYLNK"
	OPT+=("--link-dest=../$DAYLNK")
fi
if test $DEMO -eq 1
then
	OPT+=("--dry-run")
fi

#Execute the backup
echo "rsync""$SRC" "$TRG$DAY0" "${OPT[@]}"
read
sudo rsync "$SRC" "$TRG$DAY0" "${OPT[@]}" 
