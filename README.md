# yourBackup

yourBackup is a simple script wich allows you to create differential backups of your home directory once a day.
It supports multible user client combinations. The files will be stored in the folowing format:

```
+-- files
|   +-- <user>@<hostname>
|       +-- <YEAR>-<MONTH>-<DAY>
| 
+-- yourBackup
|   +-- exclude_$USER@$HOSTNAME.cfg
|
+-- yourBaakup.sh
```

## Dependencies

<a href="https://rsync.samba.org/">rsync</a>